# Blueprint
A minimal style available User Interface Component modules with for all Vue.js projects.


## Features

- Pre-Defined Vue.js UI Components that **NOT AFFECT THE DESIGNER'S WORK LIKES STYLESHEET, SKETCH FILES, PSD FILES**.
- Unstyled UI component that is super-easy to style customize.
- Support for various UI components, including accordion, drop-downs.
- Responsive by default, with a non-responsive option.


## Get Started

TBA


## Browser Support and Testing

Blueprint is tested and works in: (wants be later)

- Latest Stable: Chrome, Firefox, Safari, Edge
- Edge Legacy
- iOS 12+
- Android 6+


## Documents and Website

Later Soon


## Contact

[![](https://img.shields.io/badge/email-che5ya@naver.com-3EAF0E)](mailto:che5ya@naver.com)
[![](https://img.shields.io/badge/facebook-che5ya-4267B2)](https://facebook.com/che5ya)
[![](https://img.shields.io/badge/twitch-che5ya-6441a5)](https://www.twitch.tv/che5ya)


## License

blueprint is released under MIT license. You are free to use, modify and distribute this software, as long as the copyright header is left intact.
